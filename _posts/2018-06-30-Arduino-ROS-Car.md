---
title: Arduino ROS Car PID Control
author: Zinan
layout: post
cover: 'assets/images/car.jpg'
date:   2014-06-30
tags: BCS Lab Darmstadt
subclass: 'post tag-test tag-content'
categories: 'casper'
navigation: True
logo: 'assets/images/ghost.png'
---
### AutoCar remote control and PID control

For my HiWi job, I built the AutoCar, using a Fio v3 Arduino board, paired with XBee modules, and controlled by ROS registered in the PC.
The work is aimed towards a swarm of robot cars capable of accurate individual odometry and local sensing capabilities.
In order to achieve odometry for each individual car and follow a planned trajectory, speed encoders are used to measure travelled distance,
and an IMU sensor is added to measure the yaw angle for turning. A command can be sent via a PC terminal
to tell the car to go straight, backward at specified distance, and turning left or right at specified angle.

Now, in an ideal world, the car should go perfectly straight for 20cm when I send the "w020" command, and turn left for 90 degrees when "d090"
is sent. However in reality, due to the bias of the car and hardware noise, the car tend to tilt left when going straight; and because of 
the inertia and gyro drift, the turning tend to overshoot and becomes inaccurate over time. The inertia keeps the car turning a little bit, 
usually around +10 degrees, depending on the friction index of different surfaces, even though the PWM inputs has been written to 0 in both motors.

Gyro drift is a known problem for yaw angle estimation. This is due to the fact that gyro actually measures the angular acceleration in 3-axis,
and the integration of the near-DC term and high frequency noise will drift away from the original degree over time. Yaw angle estimation
is in particular sensity to the drift, since both roll and pitch can be estimated by the feedback from the accelerometer, where the angle 
relative to gravity is used to correct the drift, whereas yaw cannot. This can be alleviated by using an 9 DOF IMU with compass as reference,
or reset the gyro starting from 0 degree everytime when the car is known to be static, so that drift don't accumulate over time.

In the following video, "w200", i.e., going straight for 2m is first executed, followed by some other turning and driving commands.

[![](http://img.youtube.com/vi/kEJeTy2y3xE/0.jpg)](http://www.youtube.com/watch?v=kEJeTy2y3xE "AutoCar cmd")

We can see that the car has a bias to go left, about 35cm for travelling 2m straight. And turning for 90 degrees usually ended up
for going 100. In order to have a better odometry and following a trajectory, PID control is implemented. 

[![Trajectory PID Control](http://img.youtube.com/vi/-K2r4-I9QGw/0.jpg)](http://www.youtube.com/watch?v=-K2r4-I9QGw "Trajectory PID Control")

Travelling distance is fixed at 20cm per step, and the turning angle is PID controlled. The parameters of PID gains are
firstly tuned by simulation, where ROS messages regarding command and feedback is simulated, as depicted here in the project [README.md](https://gitlab.com/kongwu/Arduino#pid-control-driving-a-straight-line).
The parameters are then tested and fined tuned on the hardware with trial runs. Due to the nature of the discrete control of the
AutoCar, the trajectory tuned by PID seems a bit zig-zaggy, very different from continuous control of a smooth and
converging trajectory. However, the trajectory does stay around the traget line, with an error of ±10 deg for going 2m,
rather than the previous +35 deg of error.

### Hardware list

* [Sparkfun Fio v3](https://www.sparkfun.com/products/11520) (with Micro USB)
* Sparkfun Motor Driver - [Dual TB6612FNG](https://www.sparkfun.com/products/14451) (1A)
* 6 DOF [IMU MPU6050](https://playground.arduino.cc/Main/MPU-6050)(Acc + Gyro)
* IR Speed Sensor [LM393](https://www.voelkner.de/products/1078560/Arduino-Erweiterungs-Platine-LM393.html) x2
* 3.7 V 800mAh [Lipo Battery](https://www.amazon.de/gp/product/B076LYFZRD/ref=oh_aui_detailpage_o04_s00?ie=UTF8&psc=1) with JST connector (DOESN’T FIT!)
* 2WD Beginner [Robot Chassis](https://www.robotshop.com/de/en/2wd-beginner-robot-chassis.html)
* 1.5V AAA Battery x4 (and battery case)
* [XBee 1mW S1](https://eckstein-shop.de/SparkFun-XBee-Wireless-1mW-Trace-Antenna-Series-1-802154) (802.15.4) with Trace Antenna x2
* SparkFun [XBee Explorer Dongle](https://www.sparkfun.com/products/11697)
* [Cable Binder](https://www.amazon.de/Meister-Kabelbinder-wei%C3%9F-St%C3%BCck-7452580/dp/B017083E6Q/ref=sr_1_2?ie=UTF8&qid=1528274346&sr=8-2&keywords=kabelbinder)
* [3-Pin Slide Switch](https://eckstein-shop.de/Pololu-Mini-Slide-Switch-3-Pin-SPDT-03A-3-Pack)
* Small Breadboard
* Jumper wires, male/female pins, etc

### Circuit drawing

![alt text](../assets/images/AutoCar Schematic.png)

The chassis comes with two 6V DC motors mounted to the wheel. The motor drive is needed to control the direction of both wheels without the need to manually switch the + and - of the motor wires. The speed of the motor is controlled by writing analog values([0,255]) to the PWM pin of the board. Although the speed control is not accurate, and varies to the friction of different surfaces, the assumption of the swarm application running on a smooth surface with constant speed, should suffice for the basic needs. Otherwise PID control in the [arduino library](https://playground.arduino.cc/Code/PIDLibrary) can be used for accurate friction-invariant speed control of the carbot.

The basic motor control consists of speed sensor count feedback, to control and monitor the distance travelled, and the IMU yaw angle feedback, to control the angle of turning. Distance control is achieved by the optical encoder counting the number of slots turned on the wheel disk. And the turning-angle control is realized by a 6DOF IMU consisting of 3DOF accelerometer and gyroscope each, where sensor fusion is used for orientation estimation.

The XBee S1 module is chosen for a mesh network communication. Each spherebot in the swarm is assumed to follow a peer-to-peer communication, once falls into the neighborhood of the peers. For testing case, one XBee module can be connected to PC via a USB dongle, the other on the Fio v3 board, to remote control the robot car, and sending real time odometry information to the PC for monitoring and debugging. This can also be scaled to a swarm platform consisting of dozens of sphere-bots, where the XBee connected to the PC functions as a peer transceiver, that monitors the whole swarm by logging, plotting, debugging, etc.


